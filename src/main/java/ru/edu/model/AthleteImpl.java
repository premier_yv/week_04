package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete {
    private String firstName;
    private String lastName;
    private String country;

    /**
     * Создание конструктора класса.
     *
     * @param firstName
     * @param lastName
     * @param country
     */
    public AthleteImpl(String firstName, String lastName, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }
    //не забываем про Equals и Hachcode при сравнении двух атлетов

    /**
     * Сравнение двух атлетов по содержимому полей.
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AthleteImpl athlete = (AthleteImpl) o;
        return firstName.equals(athlete.firstName) && lastName.equals(athlete.lastName) && country.equals(athlete.country);
    }

    /**
     * Сравнение двух атлетов по хеш-коду.
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }

}
