package ru.edu.model;

import ru.edu.Competition;

import java.util.*;

public class CompetitionImpl implements Competition {

    private long id = 0;
    private long score = 0;

    // MyParticipant myParticipant;

    /**
     * Создание структуры данных participantMap.
     */
    private Map<Long, MyParticipant> participantMap = new HashMap<>();
    private Map<String, CountryParticipant> countriesMap = new TreeMap<>();

    //можно сделать свою реализацию интерфейса Athlete со ссылкой на участника
    private Set<Athlete> registeredAthlets = new HashSet<>();

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {

        if (registeredAthlets.contains(participant)) {
            throw new IllegalArgumentException("Duplicate registration");
        }
        registeredAthlets.add(participant);
        MyParticipant internalAndSecuredObject = new MyParticipant(++id, participant, score,
                participant.getCountry(), participant.getFirstName(), participant.getLastName());

        participantMap.put(internalAndSecuredObject.getId(), internalAndSecuredObject);
        countriesMap.put(internalAndSecuredObject.getAthlete().getCountry(),
                                                  internalAndSecuredObject);

        return internalAndSecuredObject.getClone();
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        MyParticipant myParticipant = participantMap.get(id);
        if (myParticipant == null) {
            throw new NullPointerException("there are no records of athletes");
        } else {
            myParticipant.countScore(score);
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score); //делегируем выполнение на метод
                                              // updateScore(long id, long score)
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        LinkedList<Participant> myParticipants = new LinkedList<>(participantMap.values());
        LinkedList<CountryParticipant> myParticipantsCountres = new LinkedList<>(participantMap.values());
        myParticipants.sort(((o1, o2) -> (o1.getScore() > o2.getScore()) ? -1 :
                (o1.getScore() < o2.getScore()) ? 1 : 0));
        for (int i = 0; i < myParticipants.size(); i++) {
            countriesMap.put(myParticipants.get(i).getAthlete().getCountry(), myParticipantsCountres.element());
        }
        // System.out.println(countriesMap.entrySet());
        return myParticipants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        LinkedList<CountryParticipant> myParticipantsCountryes = new LinkedList<>(countriesMap.values());
        return myParticipantsCountryes;
    }

    /**
     * Получение списка участников в алфавитном порядке.
     */
    public List<Participant> getParticipantsAlfabet() {
        LinkedList<Participant> MyParticipant = new LinkedList<>(participantMap.values());
        MyParticipant.sort(((o1, o2) -> (o1.getAthlete().getFirstName().equals(o2.getAthlete().getFirstName())) ? -1 :
                (o2.getAthlete().getFirstName().equals(o1.getAthlete().getFirstName())) ? 1 : 0));
        return MyParticipant;
    }

    /**
     * Получение участника.
     *
     * @return
     */
    @Override
    public int getParticipantCount() {
        LinkedList<CountryParticipant> myParticipants = new LinkedList<>(countriesMap.values());
        return myParticipants.size();
    }

    /**
     * Реализация интерфейса Participant в классе MyParticipant.
     */
    private static class MyParticipant implements Participant, CountryParticipant {
        public final Long id;
        public Athlete participant;
        private Long score;
        private String country;
        private String firstName;
        private String lastName;

        public MyParticipant(long id) {
            this.id = id;
        }

        /**
         * Создание MyParticipant.
         *
         * @param id
         * @param participant
         * @param score
         * @param country
         * @param firstName
         * @param lastName
         */

        public MyParticipant(long id, Athlete participant, Long score, String country, String firstName,
                             String lastName) {
            this.id = id;
            this.participant = participant;
            this.score = score;
            this.country = country;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация о спортсмене
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return participant;
        }

        /**
         * Название страны.
         *
         * @return название
         */
        @Override
        public String getName() {
            String name;
            name = firstName + " " + lastName;
            return name;
        }

        public String getCountry() {
            return country;
        }

        /**
         * Список участников от страны.
         *
         * @return список участников
         */
        @Override
        public List<Participant> getParticipants() {
            return null;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        public MyParticipant getClone() {
            return new MyParticipant(this.id);
        }

        public void countScore(Long score) {
            this.score += score;
        }
    }
}
