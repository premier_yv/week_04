package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CompetitionImplTest {

    /**
     * Создание экземпляра класса CompetitionImpl.
     */
    CompetitionImpl participant = new CompetitionImpl();

    @Test
    public void register() {

        participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        try {
            participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
            fail("Такой элемент уже есть");
        } catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void updateScore() {

        try {
            participant.updateScore(11, 100);
        } catch (NullPointerException e) {
        }

        participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        participant.updateScore(1, 100);

    }

    @Test
    public void testUpdateScore() {
        Participant myParticipant;

        myParticipant = participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        participant.updateScore(myParticipant, 100);
        assertEquals(100, participant.getResults().get(0).getScore());
        participant.updateScore(myParticipant, 10);
        assertEquals(110, participant.getResults().get(0).getScore());
        participant.updateScore(myParticipant, -5);
        assertEquals(105, participant.getResults().get(0).getScore());
    }

    @Test
    public void getResults() {
        Participant myParticipant;
        myParticipant = participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        participant.updateScore(myParticipant, 100);
        myParticipant = participant.register(new AthleteImpl("Sergey", "Aksenov", "Russia"));
        participant.updateScore(myParticipant, 70);
        myParticipant = participant.register(new AthleteImpl("Symon", "Estraker", "USA"));
        participant.updateScore(myParticipant, 85);
        assertEquals(100, participant.getResults().get(0).getScore());
        assertEquals(85, participant.getResults().get(1).getScore());
        assertEquals(70, participant.getResults().get(2).getScore());

    }

    @Test
    public void getParticipantsCountries() {
        Participant myParticipant;
        myParticipant = participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        participant.updateScore(myParticipant, 100);
        myParticipant = participant.register(new AthleteImpl("Valentin", "Perras", "France"));
        participant.updateScore(myParticipant, 79);
        myParticipant = participant.register(new AthleteImpl("Symon", "Estraker", "USA"));
        participant.updateScore(myParticipant, 78);
        myParticipant = participant.register(new AthleteImpl("Sergey", "Aksenov", "Russia"));
        participant.updateScore(myParticipant, 77);

      //  assertEquals(100, participant.getResults().get(0).getAthlete().getCountry() + " - "
    //                                + participant.getResults().get(0).getAthlete().getFirstName() + " "
      //                              + participant.getResults().get(0).getAthlete().getLastName());
     //   assertEquals(85, participant.getResults().get(1).getScore());
     //   assertEquals(70, participant.getResults().get(2).getScore());
        participant.getParticipantsCountries();

        for (int i = 0; i < participant.getResults().size(); i++) {
            System.out.println(participant.getResults().get(i).getAthlete().getCountry() + " - "
                    + participant.getResults().get(i).getAthlete().getFirstName() + " "
                    + participant.getResults().get(i).getAthlete().getLastName());
        }
    }

    @Test
    public void getParticipantsAlfabet() {
        Participant myParticipant;
        myParticipant = participant.register(new AthleteImpl("Nikolay", "Popov", "Russia"));
        participant.updateScore(myParticipant, 100);
        myParticipant = participant.register(new AthleteImpl("Valentin", "Perras", "France"));
        participant.updateScore(myParticipant, 79);
        myParticipant = participant.register(new AthleteImpl("Symon", "Estraker", "USA"));
        participant.updateScore(myParticipant, 78);
        myParticipant = participant.register(new AthleteImpl("Sergey", "Aksenov", "Russia"));
        participant.updateScore(myParticipant, 77);
        participant.getParticipantsAlfabet();
        for (int i = 0; i < participant.getResults().size(); i++) {
            System.out.println(participant.getResults().get(i).getAthlete().getCountry() + " - "
                    + participant.getResults().get(i).getAthlete().getFirstName() + " "
                    + participant.getResults().get(i).getAthlete().getLastName());
        }
    }

}